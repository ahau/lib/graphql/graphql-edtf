import AhauClient from 'ahau-graphql-client'

const ahauServer = require('ahau-graphql-server')
const fetch = require('node-fetch')
const gql = require('graphql-tag')
const { EdtfDate, EdtfInterval } = require('../')

const edtf = require('edtf')

function resolver (input) {
  return input
}

const BASE_PORT = 23455
let i = 0
const getPort = () => BASE_PORT + (i++)

module.exports = async function () {
  const resolvers = {

    EdtfDate,
    EdtfInterval,

    Query: {
      dateOfBirthValidString: () => '1985-04-12',
      dateOfBirthValidEdtfObject: () => edtf('1985-04-12'),
      dateOfBirthValidDateObject: () => new Date('1985-04-12'),
      dateOfBirthValidNumber: () => 482112000000,
      dateNull: () => null,
      invalidString: () => 'this is an invalid date',
      invalidObject: () => {
        return {
          date: 'this is an invalid date'
        }
      },
      invalidNumber: () => -123182312371238172397123,
      invalidEmptyString: () => '',
      invalidBoolean: () => false,

      lifespanValidString: () => '1985-04-12/1990-06-12',
      lifespanValidObject: () => edtf('1985-04-12/1990-06-12'),
      intervalNull: () => null,
      invalidIntervalString: () => 'this is an invalid interval',
      invalidIntervalObject: () => {
        return {
          date: 'this is an invalid interval'
        }
      },
      invalidIntervalNumber: () => 12344567654321
    },
    Mutation: {
      saveDate: (_, { input }) => resolver(input),
      saveInterval: (_, { input }) => resolver(input)
    }
  }

  const typeDefs = gql`
    scalar EdtfDate
    scalar EdtfInterval

    type Query {
      dateOfBirthValidString: EdtfDate
      dateOfBirthValidEdtfObject: EdtfDate
      dateOfBirthValidDateObject: EdtfDate
      dateOfBirthValidNumber: EdtfDate
      dateNull: EdtfDate
      invalidString: EdtfDate
      invalidObject: EdtfDate
      invalidNumber: EdtfDate
      invalidEmptyString: EdtfDate
      invalidBoolean: EdtfDate

      lifespanValidString: EdtfInterval
      lifespanValidObject: EdtfInterval
      intervalNull: EdtfInterval
      invalidIntervalString: EdtfInterval
      invalidIntervalObject: EdtfInterval
      invalidIntervalNumber: EdtfInterval
      
      
    },

    type Mutation {
      saveDate(input: EdtfDate): EdtfDate
      saveInterval(input: EdtfInterval): EdtfInterval
    }
  `

  const port = getPort()

  /* start server */
  const server = await ahauServer({
    context: {},
    schemas: [{ typeDefs, resolvers }],
    port
  })

  const apollo = new AhauClient(port, { fetch, isTesting: true })

  return {
    server,
    apollo
  }
}
