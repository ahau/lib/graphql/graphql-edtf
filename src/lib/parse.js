const { GraphQLError } = require('graphql')
const clone = require('lodash.clonedeep')
const edtf = require('edtf')

function edtfParse (v, opts) {
  try {
    edtf.parse(v, opts)
  } catch (err) {
    throw new GraphQLError(`Invalid EDTF date "${v}"`)
  }
}

module.exports = function parse (value, type) {
  if (value === null) return
  let v = clone(value)
  switch (typeof v) {
    case 'string':
      if (v === 'null') break
      edtfParse(v, { types: [type] })
      break
    case 'object':
      if (v instanceof Date) {
        // handle edtf date and date objects
        v = v.toISOString()
      } else {
        if (v.type && v.type === 'Interval') {
          v = value.edtf
        } else {
          throw new GraphQLError(`Expected a value of type Edtf ${type} but received: ${JSON.stringify(value)} instead.`)
        }
      }
      edtfParse(v, { types: [type] })
      break
    case 'number':
      v = new Date(v)
      if (isNaN(v.getTime())) throw new GraphQLError(`Expected value of type number to be a valid date in milliseconds. Got ${value} instead.`)
      edtfParse(v.toISOString(), { types: [type] })
      break
    default:
      throw new GraphQLError(`Value is not an instance of date or of type string, object, number or null. Got ${JSON.stringify(value)} instead`)
  }
}
